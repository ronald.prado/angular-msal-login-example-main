import { AuthenticationResult } from '@azure/msal-browser';
import { MsalService } from '@azure/msal-angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'My Microsoft Login- Example';
  isIframe = false;

  constructor(private authService: MsalService, private routes: Router) {}

  ngOnInit(): void {
    this.isIframe = window !== window.parent && !window.opener;
    this.authService.instance.handleRedirectPromise().then((res) => {
      if (res != null && res.account != null) {
        this.authService.instance.setActiveAccount(res.account);
      }
    });
  }

  isLoggedIn(): boolean {
    return this.authService.instance.getActiveAccount() != null;
  }

  login() {
    this.authService
      .loginPopup()
      .subscribe((response: AuthenticationResult) => {
        this.authService.instance.setActiveAccount(response.account);
        console.log('REGRESE AL METODO login en app.component (POC)');
        // window.location.href = '/vta';
        this.routes.navigate(['v1/oauth/callback']);
      });
  }

  logout() {
    this.authService.logout();
    if (localStorage.getItem('infoAccessToken')) {
      localStorage.removeItem('infoAccessToken');
    }
  }
}
