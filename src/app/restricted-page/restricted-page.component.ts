import { MsalService } from '@azure/msal-angular';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const GRAPH_ENDPOINT = 'https://graph.microsoft.com/v1.0/me/';
const GRAPH_ENDPOINT2 = 'https://graph.microsoft.com/v1.0/me';

@Component({
  selector: 'app-restricted-page',
  templateUrl: './restricted-page.component.html',
  styleUrls: ['./restricted-page.component.css'],
})
export class RestrictedPageComponent implements OnInit {
  profile: any;
  profile2: any;
  oidData: string;

  constructor(private authService: MsalService, private http: HttpClient) {}

  getName(): string {
    if (this.authService.instance.getActiveAccount() == null) {
      return 'unknown';
    }

    return this.authService.instance.getActiveAccount().name;
  }

  ngOnInit(): void {
    // window.location.href = 'vta/panel';

    const infoAccount = this.authService.instance.getActiveAccount();
    this.oidData = infoAccount?.idTokenClaims['oid'];
    // console.log('oidData: ', this.oidData);

    this.getProfile();
  }

  getProfile() {
    // {{oid}}/onPremisesExtensionAttributes
    const accessToken = JSON.parse(localStorage.getItem('infoAccessToken'));

    const headerData = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + accessToken.secret,
    });

    console.log('ACCESS TOKEN: ', accessToken.secret);

    console.log(
      '------------------------------------------------------------------------------------------'
    );

    // https://graph.microsoft.com/v1.0/users/{{oid}}/onPremisesExtensionAttributes

    this.http
      .get(`${GRAPH_ENDPOINT}onPremisesExtensionAttributes`, {
        headers: headerData,
      })
      .subscribe((profile) => {
        this.profile = profile;

        console.log(
          'RESULT FROM ENDPOINT (https://graph.microsoft.com/v1.0/me/onPremisesExtensionAttributes): ',
          this.profile
        );
        console.log(
          '------------------------------------------------------------------------------------------'
        );
      });

    // https://graph.microsoft.com/v1.0/me

    this.http
      .get(`${GRAPH_ENDPOINT2}`, {
        headers: headerData,
      })
      .subscribe((profile) => {
        this.profile2 = profile;

        console.log(
          'RESULT FROM ENDPOINT (https://graph.microsoft.com/v1.0/me): ',
          this.profile2
        );
        console.log(
          '------------------------------------------------------------------------------------------'
        );
      });

    // https://graph.microsoft.com/v1.0/me/memberOf

    this.http
      .get(`${GRAPH_ENDPOINT2}/memberOf`, {
        headers: headerData,
      })
      .subscribe((profile) => {
        const profile3 = profile;

        console.log(
          'RESULT FROM ENDPOINT (https://graph.microsoft.com/v1.0/me/memberOf): ',
          profile3
        );
        console.log(
          '------------------------------------------------------------------------------------------'
        );
      });

    // https://graph.microsoft.com/v1.0/me?$select=employeeid,country,displayName

    this.http
      .get(`${GRAPH_ENDPOINT2}?$select=employeeid,country,displayName`, {
        headers: headerData,
      })
      .subscribe((profile) => {
        const profile4 = profile;

        console.log(
          'RESULT FROM ENDPOINT (https://graph.microsoft.com/v1.0/me?$select=employeeid,country,displayName): ',
          profile4
        );
        console.log(
          '------------------------------------------------------------------------------------------'
        );
      });

    // https://graph.microsoft.com/v1.0/me/manager
  }
}
