import { MicrosoftLoginGuard } from './microsoft-login.guard';
import { RestrictedPageComponent } from './restricted-page/restricted-page.component';
import { PublicPageComponent } from './public-page/public-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MsalGuard } from '@azure/msal-angular';

const routes: Routes = [
  { path: 'public-page', component: PublicPageComponent },
  { path: 'v1/oauth/login', component: LoginComponent },
  {
    path: 'v1/oauth/callback',
    component: RestrictedPageComponent,
    canActivate: [MicrosoftLoginGuard, MsalGuard],
  },
  {
    path: 'restricted-page',
    component: RestrictedPageComponent,
  },
  // { path: '**', component: LoginComponent },
  // { path: '', redirectTo: 'public-page', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
