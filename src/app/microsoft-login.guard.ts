import { MsalService } from '@azure/msal-angular';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MicrosoftLoginGuard implements CanActivate {
  constructor(private authService: MsalService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.authService.instance.getActiveAccount() == null) {
      console.log('not logged in!');
      return false;
    }

    const infoStorage = localStorage;
    // console.log('infoStorage: ', infoStorage);

    for (const key in infoStorage) {
      if (Object.prototype.hasOwnProperty.call(infoStorage, key)) {
        if (key.includes('net-accesstoken')) {
          // console.log('key: ', key);
          const element = infoStorage[key];
          console.log(JSON.parse(element), ' | ', typeof element);
          localStorage.setItem('infoAccessToken', element);
        }
      }
    }

    // const infoTokenKey = localStorage.key(0);
    // console.log('infoTokenKey: ', infoTokenKey);
    // localStorage.setItem('infoAccessToken', localStorage.getItem(infoTokenKey));

    return true;
  }
}
